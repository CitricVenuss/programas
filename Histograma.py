from cmath import log, pi, sqrt
import numpy as np
import matplotlib.pyplot as plt
import math 

def histoGrama():       

    list = []

    for i in range (10000):
        list.append(np.random.random())

    plt.hist(list)
    plt.show()

def histogramaNormal():

    list = []
    z0 = 0
    z1 = 0

    for i in range (10000):
        list.append(np.random.random())

    aux = []

    for i in range (0,len(list),2):

        z0 = sqrt(-2*log(list[i-1])) * math.cos(2 * pi * list[i])
        aux.insert(i-1,z0)

        z1 = sqrt(-2*log(list[i-1])) * math.sin(2 * pi * list[i])
        aux.insert(i,z1)

    plt.hist(aux)
    plt.show()


histoGrama()
histogramaNormal()